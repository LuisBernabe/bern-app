package com.brn.bernapp.shared.framework.config.retrofit.response

import com.google.gson.annotations.SerializedName

data class MediaBulkResponseDto(
    val page: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int,
    val results: List<MediaResultDto>
)

open class MediaResultDto(
    val id: Int,
    @SerializedName("original_language") val language: String,
    val title: String, // movie
    val name: String, // tvShow
    val overview: String,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("first_air_date") val firstAirDate: String,
    @SerializedName("poster_path") val posterUrl: String,
    @SerializedName("backdrop_path") val backdropPath: String,
    @SerializedName("media_type") val mediaType: String,
) {
    override fun toString(): String {
        return "id: $id \ntitle: $title\nposterUrl:$posterUrl\nreleaseDate: $releaseDate"
    }
}