package com.brn.bernapp.shared.framework.data.implementations.repository

import com.brn.bernapp.shared.data.datasource.TvShowDS
import com.brn.bernapp.shared.data.repository.TvShowRepository
import com.brn.bernapp.shared.domain.model.TvShow
import javax.inject.Inject

class TvShowRepositoryImpl @Inject constructor(private val tvShowDS: TvShowDS) : TvShowRepository {
    override suspend fun getTrendingTvShows(): List<TvShow> {
        val result = tvShowDS.getTrendingTvShows()
        return result
    }


}