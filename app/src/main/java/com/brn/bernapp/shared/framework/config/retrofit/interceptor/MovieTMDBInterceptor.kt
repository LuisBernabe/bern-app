package com.brn.bernapp.shared.framework.config.retrofit.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class MovieTMDBInterceptor : Interceptor {
    companion object {
        const val AUTHORIZATION = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val requestToCustomize = originalRequest.newBuilder()

        requestToCustomize.addHeader(
            AUTHORIZATION,
            "Bearer $TMDBtkn"
        )
        val requestCustomized = requestToCustomize.build()

        return chain.proceed(requestCustomized)
    }

}