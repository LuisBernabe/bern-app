package com.brn.bernapp.shared.framework.data.implementations.datasource

import com.brn.bernapp.shared.framework.config.retrofit.apiServices.MovieApiService
import com.brn.bernapp.shared.data.datasource.SearcherDS
import com.brn.bernapp.shared.framework.config.retrofit.response.MediaBulkResponseDto
import javax.inject.Inject

class SearcherDataSourceImpl @Inject constructor(private val movieApiService: MovieApiService) :
    SearcherDS {
    override suspend fun getRemoteSearch(): MediaBulkResponseDto? {
        val call = movieApiService.getMoviesToDiscover(
            1,
            "popularity.desc"
        )

        return call.body()
    }
}