package com.brn.bernapp.shared.framework.di

import android.content.Context
import androidx.room.Room
import com.brn.bernapp.shared.framework.config.room.bernDB.BernDatabase
import com.brn.bernapp.shared.framework.config.room.bernDB.daos.MediaDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun providesBernDatabase(@ApplicationContext appContext: Context): BernDatabase {
        return Room.databaseBuilder(appContext, BernDatabase::class.java, "bern-db").build()
    }

    @Provides
    fun providesMediaDao(bernDatabase: BernDatabase): MediaDao {
        return bernDatabase.mediaDao()
    }
}