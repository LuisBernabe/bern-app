package com.brn.bernapp.shared.framework.data.implementations.repository

import com.brn.bernapp.shared.data.datasource.MovieDS
import com.brn.bernapp.shared.data.datasource.local.MediaLocalDS
import com.brn.bernapp.shared.data.repository.MovieRepository
import com.brn.bernapp.shared.domain.model.Movie
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieDS: MovieDS,
    private val mediaLocalDS: MediaLocalDS
) : MovieRepository {
    override suspend fun getMovies(numberPage: Int): List<Movie> {
        return movieDS.getMovies(numberPage)
    }

    override suspend fun getTrendingMovies(): List<Movie> {
        val result = movieDS.getTrendingMovies()
        return result
    }

    override suspend fun storeMovieLocal(movie: Movie) {
        mediaLocalDS.insertMedias(movie)
    }

    override suspend fun getAllMoviesStoredLocally(): List<Movie> {

        return mediaLocalDS.getAllMovies()
    }
}