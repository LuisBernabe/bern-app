package com.brn.bernapp.shared.data.datasource

import com.brn.bernapp.shared.framework.config.retrofit.response.MediaBulkResponseDto

interface SearcherDS {

    suspend fun getRemoteSearch(): MediaBulkResponseDto?
}