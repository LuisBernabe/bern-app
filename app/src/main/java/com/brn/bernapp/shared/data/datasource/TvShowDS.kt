package com.brn.bernapp.shared.data.datasource

import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.domain.model.TvShow

interface TvShowDS {
    suspend fun getTrendingTvShows(): List<TvShow>

}