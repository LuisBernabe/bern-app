package com.brn.bernapp.shared.domain.model

open class Movie(
    override var id: Int,
    override var language: String,
    var title: String,
    override var overview: String,
    var releaseDate: String,
    override var posterUrl: String,
    override val backdropPath: String,
    override val mediaType: String?
) : Media(id, language, overview, posterUrl, backdropPath, mediaType)
