package com.brn.bernapp.shared.framework.di

import com.brn.bernapp.shared.framework.data.implementations.repository.MovieRepositoryImpl
import com.brn.bernapp.shared.framework.data.implementations.repository.SearcherRepositoryImpl
import com.brn.bernapp.shared.data.repository.MovieRepository
import com.brn.bernapp.shared.data.repository.SearcherRepository
import com.brn.bernapp.shared.data.repository.TvShowRepository
import com.brn.bernapp.shared.framework.data.implementations.repository.TvShowRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindSearcherRepository(searcherRepositoryImpl: SearcherRepositoryImpl): SearcherRepository

    @Binds
    abstract fun bindMovieRepository(movieRepositoryImpl: MovieRepositoryImpl): MovieRepository

    @Binds
    abstract fun bindTvShowRepository(tvShowRepositoryImpl: TvShowRepositoryImpl): TvShowRepository
}