package com.brn.bernapp.shared.framework.config.retrofit.apiServices

import com.brn.bernapp.shared.framework.config.retrofit.response.MediaBulkResponseDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApiService {

    @GET("discover/movie")
    suspend fun getMoviesToDiscover(
        @Query("page") page: Int,
        @Query("sort_by") sortBy: String,
    ): Response<MediaBulkResponseDto>

    @GET("trending/movie/day")
    suspend fun getTrendingMovies(): Response<MediaBulkResponseDto>

    @GET("trending/tv/day")
    suspend fun getTrendingTvShows(): Response<MediaBulkResponseDto>

}