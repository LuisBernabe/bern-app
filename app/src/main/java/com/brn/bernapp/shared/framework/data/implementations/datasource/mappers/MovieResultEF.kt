package com.brn.bernapp.shared.framework.data.implementations.datasource.mappers

import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.domain.model.TvShow
import com.brn.bernapp.shared.framework.config.retrofit.response.MediaResultDto

fun MediaResultDto.toMovie(): Movie {
    return Movie(
        this.id,
        this.language,
        this.title,
        this.overview,
        this.releaseDate,
        this.posterUrl,
        this.backdropPath,
        this.mediaType
    )
}

fun MediaResultDto.toTvShow(): TvShow {
    return TvShow(
        this.id,
        this.language,
        this.name,
        this.overview,
        this.firstAirDate,
        this.posterUrl,
        this.backdropPath,
        this.mediaType
    )
}