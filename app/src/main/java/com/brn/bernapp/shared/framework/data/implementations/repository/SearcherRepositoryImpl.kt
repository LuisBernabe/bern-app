package com.brn.bernapp.shared.framework.data.implementations.repository

import com.brn.bernapp.shared.data.datasource.SearcherDS
import com.brn.bernapp.shared.data.repository.SearcherRepository
import com.brn.bernapp.shared.framework.config.retrofit.response.MediaBulkResponseDto
import javax.inject.Inject

class SearcherRepositoryImpl @Inject constructor(private val searcherDS: SearcherDS) :
    SearcherRepository {
    override suspend fun getRemoteSearch(): MediaBulkResponseDto? {
        return searcherDS.getRemoteSearch()
    }
}