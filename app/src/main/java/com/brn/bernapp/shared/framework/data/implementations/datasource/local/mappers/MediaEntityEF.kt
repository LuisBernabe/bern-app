package com.brn.bernapp.shared.framework.data.implementations.datasource.local.mappers

import com.brn.bernapp.shared.domain.model.Media
import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.framework.config.room.bernDB.entities.MediaEntity

fun MediaEntity.toMedia(): Media {
    return Media(
        this.id,
        this.language,
        this.overview,
        this.posterUrl,
        this.backdropPath,
        this.mediaType
    )
}

fun MediaEntity.toMovie(): Movie {
    return Movie(
        this.id,
        this.language,
        "",
        this.overview,
        "",
        this.posterUrl,
        this.backdropPath,
        this.mediaType
    )
}