package com.brn.bernapp.shared.framework.presentation.extensionFunctions

import android.widget.ImageView
import coil.load
import com.brn.bernapp.R
import com.brn.bernapp.shared.framework.presentation.constants.GlobalConstants

fun ImageView.loadFromTMDB(path: String) {
    val size = "/w780"
    this.load(GlobalConstants.BASE_TMDB_URL_IMAGE + size + path) {
        placeholder(R.drawable.baseline_image_24)
        error(R.drawable.baseline_image_not_supported_24)
    }
}