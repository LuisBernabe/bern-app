package com.brn.bernapp.shared.data.datasource

import com.brn.bernapp.shared.domain.model.Movie

interface MovieDS {

    suspend fun getMovies(numberPage: Int): List<Movie>
    suspend fun getTrendingMovies(): List<Movie>


}