package com.brn.bernapp.shared.domain.model

open class Media(
    open var id: Int,
    open var language: String,
    open var overview: String,
    open var posterUrl: String,
    open val backdropPath: String,
    open val mediaType: String? = ""
)
