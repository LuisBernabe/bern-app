package com.brn.bernapp.shared.framework.config.room.bernDB.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.brn.bernapp.shared.framework.config.room.bernDB.entities.MediaEntity

@Dao
interface MediaDao {
    @Query("SELECT * FROM MEDIA")
    suspend fun getAll(): List<MediaEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMedias(vararg mediaEntities: MediaEntity)

    @Query("SELECT * FROM MEDIA WHERE id = :id")
    fun findById(id: Int): MediaEntity
}