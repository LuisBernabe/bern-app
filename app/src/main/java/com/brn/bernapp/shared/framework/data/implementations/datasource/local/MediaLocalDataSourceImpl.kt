package com.brn.bernapp.shared.framework.data.implementations.datasource.local

import android.util.Log
import com.brn.bernapp.shared.data.datasource.local.MediaLocalDS
import com.brn.bernapp.shared.domain.model.Media
import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.framework.config.room.bernDB.daos.MediaDao
import com.brn.bernapp.shared.framework.data.implementations.datasource.local.mappers.toMedia
import com.brn.bernapp.shared.framework.data.implementations.datasource.local.mappers.toMediaEntity
import com.brn.bernapp.shared.framework.data.implementations.datasource.local.mappers.toMovie
import javax.inject.Inject

class MediaLocalDataSourceImpl @Inject constructor(private val mediaDao: MediaDao) : MediaLocalDS {
    override suspend fun insertMedias(media: Media) {
        val mediaEntity = media.toMediaEntity()
        mediaDao.insertMedias(mediaEntity)
        Log.i(TAG, "mediaEntity id inserted: ${mediaEntity.id}")
    }

    override suspend fun getAllMovies(): List<Movie> {
        val allMedia = mediaDao.getAll()
        return allMedia.map { it.toMovie() }
    }


    companion object {
        const val TAG = "MediaLocalDataSourceImpl"
    }
}