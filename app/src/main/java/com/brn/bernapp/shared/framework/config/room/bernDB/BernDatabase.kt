package com.brn.bernapp.shared.framework.config.room.bernDB

import androidx.room.Database
import androidx.room.RoomDatabase
import com.brn.bernapp.shared.framework.config.room.bernDB.daos.MediaDao
import com.brn.bernapp.shared.framework.config.room.bernDB.entities.MediaEntity

@Database(entities = [MediaEntity::class], version = 1)
abstract class BernDatabase : RoomDatabase() {
    abstract fun mediaDao(): MediaDao
}