package com.brn.bernapp.shared.data.repository

import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.domain.model.TvShow

interface TvShowRepository {
    suspend fun getTrendingTvShows(): List<TvShow>

}