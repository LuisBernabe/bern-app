package com.brn.bernapp.shared.framework.data.implementations.datasource

import android.util.Log
import com.brn.bernapp.shared.framework.config.retrofit.apiServices.MovieApiService
import com.brn.bernapp.shared.data.datasource.MovieDS
import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.shared.framework.data.implementations.datasource.mappers.toMovie
import javax.inject.Inject

class MovieDataSourceImpl @Inject constructor(private val movieApiService: MovieApiService) :
    MovieDS {
    override suspend fun getMovies(numberPage: Int): List<Movie> {
        try {
            val call = movieApiService.getMoviesToDiscover(
                numberPage,
                "popularity.desc"
            )
            val movies = call.body()?.let { mediaBulkResponseDto ->
               return mediaBulkResponseDto.results.map { it.toMovie() }
            }.run {
                listOf<Movie>()
            }

            return movies
        } catch (e: Exception) {
            Log.e(this.javaClass.toGenericString(), e.toString())
            return listOf()
        }
    }

    override suspend fun getTrendingMovies(): List<Movie> {
        try {
            val call = movieApiService.getTrendingMovies()
            val trendingMovies = call.body()?.let { mediaBulkResponseDto ->
                return mediaBulkResponseDto.results.map { it.toMovie() }
            }.run {
                listOf<Movie>()
            }

            return trendingMovies
        } catch (e: Exception) {
            Log.e(this.javaClass.toGenericString(), e.toString())
            return listOf()
        }
    }


}