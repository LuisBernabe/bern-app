package com.brn.bernapp.shared.framework.data.implementations.datasource.local.mappers

import com.brn.bernapp.shared.domain.model.Media
import com.brn.bernapp.shared.framework.config.room.bernDB.entities.MediaEntity

fun Media.toMediaEntity(): MediaEntity {
    return MediaEntity(
        this.id,
        this.language,
        this.overview,
        this.posterUrl,
        this.backdropPath,
        this.mediaType
    )
}