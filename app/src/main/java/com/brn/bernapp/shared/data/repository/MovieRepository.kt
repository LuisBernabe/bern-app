package com.brn.bernapp.shared.data.repository

import com.brn.bernapp.shared.domain.model.Movie

interface MovieRepository {

    suspend fun getMovies(numberPage: Int): List<Movie>

    suspend fun getTrendingMovies(): List<Movie>

    suspend fun storeMovieLocal(movie: Movie)

    suspend fun getAllMoviesStoredLocally(): List<Movie>

}