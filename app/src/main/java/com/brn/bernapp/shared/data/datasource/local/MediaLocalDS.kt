package com.brn.bernapp.shared.data.datasource.local

import com.brn.bernapp.shared.domain.model.Media
import com.brn.bernapp.shared.domain.model.Movie

interface MediaLocalDS {

    suspend fun insertMedias(media: Media)

    suspend fun getAllMovies(): List<Movie>
}