package com.brn.bernapp.shared.framework.di

import com.brn.bernapp.shared.framework.data.implementations.datasource.MovieDataSourceImpl
import com.brn.bernapp.shared.framework.data.implementations.datasource.SearcherDataSourceImpl
import com.brn.bernapp.shared.data.datasource.MovieDS
import com.brn.bernapp.shared.data.datasource.SearcherDS
import com.brn.bernapp.shared.data.datasource.TvShowDS
import com.brn.bernapp.shared.data.datasource.local.MediaLocalDS
import com.brn.bernapp.shared.framework.data.implementations.datasource.TvShowDataSourceImpl
import com.brn.bernapp.shared.framework.data.implementations.datasource.local.MediaLocalDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun bindSearcherDataSource(searcherDataSourceImpl: SearcherDataSourceImpl): SearcherDS

    @Binds
    abstract fun bindMovieDataSource(movieDataSourceImpl: MovieDataSourceImpl): MovieDS

    @Binds
    abstract fun bindTvShowDataSource(tvShowDataSourceImpl: TvShowDataSourceImpl): TvShowDS

    @Binds
    abstract fun bindMediaLocalDataSource(mediaLocalDataSourceImpl: MediaLocalDataSourceImpl): MediaLocalDS
}