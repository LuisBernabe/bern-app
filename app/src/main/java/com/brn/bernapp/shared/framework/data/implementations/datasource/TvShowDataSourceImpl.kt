package com.brn.bernapp.shared.framework.data.implementations.datasource

import android.util.Log
import com.brn.bernapp.shared.data.datasource.TvShowDS
import com.brn.bernapp.shared.domain.model.TvShow
import com.brn.bernapp.shared.framework.config.retrofit.apiServices.MovieApiService
import com.brn.bernapp.shared.framework.data.implementations.datasource.mappers.toTvShow
import javax.inject.Inject

class TvShowDataSourceImpl @Inject constructor(private val movieApiService: MovieApiService) :
    TvShowDS {
    override suspend fun getTrendingTvShows(): List<TvShow> {
        try {
            val call = movieApiService.getTrendingTvShows()
            val trendingTvShows = call.body()?.let { mediaBulkResponseDto ->
                return mediaBulkResponseDto.results.map { it.toTvShow() }
            }.run {
                listOf<TvShow>()
            }
            return trendingTvShows
        } catch (e: Exception) {
            Log.e(this.javaClass.toGenericString(), e.toString())
            return listOf()
        }
    }


}