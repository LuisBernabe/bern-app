package com.brn.bernapp.shared.data.repository

import com.brn.bernapp.shared.framework.config.retrofit.response.MediaBulkResponseDto

interface SearcherRepository {
    suspend fun getRemoteSearch(): MediaBulkResponseDto?
}