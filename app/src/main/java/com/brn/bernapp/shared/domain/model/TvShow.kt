package com.brn.bernapp.shared.domain.model

open class TvShow(
    override var id: Int,
    override var language: String,
    var name: String,
    override var overview: String,
    var firstAirDate: String,
    override var posterUrl: String,
    override val backdropPath: String,
    override val mediaType: String?
) : Media(id, language, overview, posterUrl, backdropPath, mediaType)