package com.brn.bernapp.shared.framework.presentation.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    protected val _showLoaderMLD = MutableLiveData<Boolean>(false)
    val showLoader = _showLoaderMLD
}