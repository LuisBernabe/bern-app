package com.brn.bernapp.shared.framework.di

import com.brn.bernapp.shared.framework.config.retrofit.apiServices.MovieApiService
import com.brn.bernapp.shared.framework.config.retrofit.interceptor.MovieTMDBInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {
    @Provides
    @Singleton
    fun providesTMDBClient(): OkHttpClient {
        val interceptor = MovieTMDBInterceptor()

        return OkHttpClient.Builder().addNetworkInterceptor(interceptor).build()
    }

    @Provides
    fun provideMovieApiService(): MovieApiService {
        return Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create()).client(providesTMDBClient())
            .build()
            .create(MovieApiService::class.java)
    }
}