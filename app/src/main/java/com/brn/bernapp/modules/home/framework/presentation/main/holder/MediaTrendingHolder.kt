package com.brn.bernapp.modules.home.framework.presentation.main.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.brn.bernapp.databinding.TrendingMovieCarouselItemBinding
import com.brn.bernapp.shared.domain.model.Media
import com.brn.bernapp.shared.framework.presentation.extensionFunctions.loadFromTMDB
import com.brn.bernapp.shared.domain.model.Movie

class MediaTrendingHolder(private val binding: TrendingMovieCarouselItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun binding(media: Media) {
        binding.carouselImagePosterView.loadFromTMDB(media.posterUrl)
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): MediaTrendingHolder {
            val inflater = LayoutInflater.from(parent.context)
            return MediaTrendingHolder(
                TrendingMovieCarouselItemBinding.inflate(
                    inflater,
                    parent,
                    false
                )
            )
        }
    }
}