package com.brn.bernapp.modules.home.framework.presentation.main.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.brn.bernapp.shared.framework.presentation.base.BaseViewModel
import com.brn.bernapp.shared.domain.model.Movie
import com.brn.bernapp.modules.home.usecases.GetTrendingMoviesUseCase
import com.brn.bernapp.modules.home.usecases.GetTrendingTvShowsUseCase
import com.brn.bernapp.shared.domain.model.Media
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getTrendingMoviesUseCase: GetTrendingMoviesUseCase,
    private val getTrendingTvShowsUseCase: GetTrendingTvShowsUseCase
) :
    BaseViewModel() {
    private val _trendingMoviesMLD = MutableLiveData<List<Media>>()
    val trendingMovies = _trendingMoviesMLD
    fun setup() {
        retrieveTrendingMovies()
    }

    fun retrieveTrendingMovies() {
        _showLoaderMLD.value = true
        viewModelScope.launch {
            val res = getTrendingMoviesUseCase.execute()
            _showLoaderMLD.value = false
            _trendingMoviesMLD.value = res
        }
    }

    fun retrieveTrendingTvShows() {
        _showLoaderMLD.value = true
        viewModelScope.launch {
            val res = getTrendingTvShowsUseCase.execute()
            _showLoaderMLD.value = false
            _trendingMoviesMLD.value = res
        }
    }

}