package com.brn.bernapp.modules.searcher.framework.presentation.main.viewModel

import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.brn.bernapp.modules.searcher.framework.presentation.main.paging.SearchTrendingMoviesPagingSource
import com.brn.bernapp.shared.framework.presentation.base.BaseViewModel
import com.brn.bernapp.modules.searcher.usecases.GetMoviesToDiscoverUseCase
import com.brn.bernapp.modules.searcher.usecases.StoreMovieLocalUseCase
import com.brn.bernapp.shared.domain.model.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearcherViewModel @Inject constructor(
    private val getMoviesToDiscoverUseCase: GetMoviesToDiscoverUseCase,
    private val storeMovieLocalUseCase: StoreMovieLocalUseCase
) : BaseViewModel() {


    fun setup() {
        //getMoviesToDiscover()
        //getMoviesToDiscoverPaging()
    }

    fun downloadMovieInfo(movie: Movie) {
        viewModelScope.launch {
            storeMovieLocalUseCase.execute(movie)
        }
    }

    val movieListPaging = Pager(PagingConfig(pageSize = 10)) {
        SearchTrendingMoviesPagingSource(getMoviesToDiscoverUseCase)
    }.liveData.cachedIn(viewModelScope)

    companion object {
        private const val TAG = "SEARCHER-VM"
    }
}