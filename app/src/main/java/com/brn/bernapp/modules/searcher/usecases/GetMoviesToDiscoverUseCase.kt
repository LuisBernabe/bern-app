package com.brn.bernapp.modules.searcher.usecases

import com.brn.bernapp.shared.data.repository.MovieRepository
import com.brn.bernapp.shared.domain.model.Movie
import javax.inject.Inject

class GetMoviesToDiscoverUseCase @Inject constructor(private val movieRepository: MovieRepository) {

    suspend fun execute(numberPage: Int = 1): List<Movie> {
        return movieRepository.getMovies(numberPage)
    }
}