package com.brn.bernapp.modules.home.usecases

import com.brn.bernapp.shared.data.repository.TvShowRepository
import com.brn.bernapp.shared.domain.model.TvShow
import javax.inject.Inject

class GetTrendingTvShowsUseCase @Inject constructor(private val tvShowRepository: TvShowRepository) {
    suspend fun execute(): List<TvShow> {
        return tvShowRepository.getTrendingTvShows()
    }
}