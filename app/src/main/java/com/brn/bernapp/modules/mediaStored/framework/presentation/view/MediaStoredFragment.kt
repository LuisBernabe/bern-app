package com.brn.bernapp.modules.mediaStored.framework.presentation.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.brn.bernapp.R
import com.brn.bernapp.databinding.FragmentMediaStoredBinding
import com.brn.bernapp.modules.mediaStored.framework.presentation.viewModel.MediaStoredViewModel
import com.brn.bernapp.shared.framework.presentation.base.BaseFragment

class MediaStoredFragment : BaseFragment<FragmentMediaStoredBinding>() {
    private val viewModel: MediaStoredViewModel by viewModels()
    override fun startBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentMediaStoredBinding = FragmentMediaStoredBinding.inflate(layoutInflater)


    override fun initView() {
        super.initView()
    }
}
