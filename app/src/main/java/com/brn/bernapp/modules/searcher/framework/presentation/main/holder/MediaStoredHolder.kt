package com.brn.bernapp.modules.searcher.framework.presentation.main.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.brn.bernapp.databinding.ItemMediaStoredBinding
import com.brn.bernapp.shared.domain.model.Media

class MediaStoredHolder(private val binding: ItemMediaStoredBinding) : ViewHolder(binding.root) {


    fun bind(media: Media) {
        binding.tvMediaId.text = media.id.toString()
        binding.tvUrl.text = media.backdropPath
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): MediaStoredHolder {
            val inflater = LayoutInflater.from(parent.context)
            return MediaStoredHolder(ItemMediaStoredBinding.inflate(inflater, parent, false))
        }
    }

}