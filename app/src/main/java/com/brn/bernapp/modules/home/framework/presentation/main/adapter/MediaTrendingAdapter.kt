package com.brn.bernapp.modules.home.framework.presentation.main.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.brn.bernapp.modules.home.framework.presentation.main.holder.MediaTrendingHolder
import com.brn.bernapp.shared.domain.model.Media

class MediaTrendingAdapter : ListAdapter<Media, MediaTrendingHolder>(MovieTrendingDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaTrendingHolder {
        return MediaTrendingHolder.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: MediaTrendingHolder, position: Int) {
        val mediaTrending = getItem(position)
        holder.binding(mediaTrending)
    }
}

object MovieTrendingDiffCallback : DiffUtil.ItemCallback<Media>() {
    override fun areItemsTheSame(oldItem: Media, newItem: Media): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Media, newItem: Media): Boolean {
        return oldItem.id == newItem.id && oldItem.backdropPath == newItem.backdropPath
    }

}