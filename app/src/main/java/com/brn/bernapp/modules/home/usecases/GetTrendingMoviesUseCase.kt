package com.brn.bernapp.modules.home.usecases

import com.brn.bernapp.shared.data.repository.MovieRepository
import com.brn.bernapp.shared.domain.model.Movie
import javax.inject.Inject

class GetTrendingMoviesUseCase @Inject constructor(private val movieRepository: MovieRepository) {

    suspend fun execute(): List<Movie> {
        return movieRepository.getTrendingMovies()
    }
}