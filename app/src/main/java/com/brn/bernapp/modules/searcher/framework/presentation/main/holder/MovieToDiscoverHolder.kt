package com.brn.bernapp.modules.searcher.framework.presentation.main.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.brn.bernapp.databinding.MovieToDiscoverItemBinding
import com.brn.bernapp.shared.framework.presentation.extensionFunctions.loadFromTMDB
import com.brn.bernapp.shared.domain.model.Movie

class MovieToDiscoverHolder(private val binding: MovieToDiscoverItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun binding(movie: Movie, onClickListener: (movieId: Movie) -> Unit) {
        binding.tvMovieTitle.text = movie.title
        binding.tvMovieOverview.text = movie.overview
        binding.ivMoviePhoto.loadFromTMDB(movie.backdropPath)
        binding.cardMovie.setOnClickListener {
            onClickListener(movie)
        }
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): MovieToDiscoverHolder {
            val inflater = LayoutInflater.from(parent.context)
            return MovieToDiscoverHolder(
                MovieToDiscoverItemBinding.inflate(
                    inflater,
                    parent,
                    false
                )
            )
        }
    }
}