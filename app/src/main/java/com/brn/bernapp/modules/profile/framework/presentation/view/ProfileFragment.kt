package com.brn.bernapp.modules.profile.framework.presentation.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.brn.bernapp.R
import com.brn.bernapp.databinding.FragmentProfileBinding
import com.brn.bernapp.modules.profile.framework.presentation.viewModel.ProfileViewModel
import com.brn.bernapp.shared.framework.presentation.base.BaseFragment


class ProfileFragment : BaseFragment<FragmentProfileBinding>() {
    val viewModel: ProfileViewModel by viewModels()

    override fun startBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentProfileBinding = FragmentProfileBinding.inflate(layoutInflater)


    override fun initView() {
        super.initView()

        binding.btnMyMediaStored.setOnClickListener {
            val action = R.id.action_profileFragment_to_mediaStoredFragment
            findNavController().navigate(action)
        }
    }
}