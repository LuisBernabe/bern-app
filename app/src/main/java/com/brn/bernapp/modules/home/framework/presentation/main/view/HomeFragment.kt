package com.brn.bernapp.modules.home.framework.presentation.main.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.brn.bernapp.R
import com.brn.bernapp.shared.framework.presentation.base.BaseFragment
import com.brn.bernapp.databinding.FragmentHomeBinding
import com.brn.bernapp.modules.home.framework.presentation.main.adapter.MediaTrendingAdapter
import com.brn.bernapp.modules.home.framework.presentation.main.viewModel.HomeViewModel
import com.google.android.material.carousel.CarouselLayoutManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var mediaTrendingAdapter: MediaTrendingAdapter

    override fun startBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentHomeBinding = FragmentHomeBinding.inflate(layoutInflater)

    override fun initView() {
        super.initView()
        viewModel.setup()
        binding.btgSelector.check(R.id.btn_movies)
        setupRV()
        handleButtonToggleListener()
        viewModel.showLoader.observe(this) {
            handleLoader(it)
        }
        viewModel.trendingMovies.observe(this) {
            Log.i("HOME-FRAGMENT", "NUMBER OF ITEMS: ${it.size}")
            binding.rvCarouselTrendingMovies.scrollToPosition(0)
            mediaTrendingAdapter.submitList(it)
        }

    }

    private fun setupRV() {
        binding.rvCarouselTrendingMovies.layoutManager =
            CarouselLayoutManager()
        //     val snapHelper = CarouselSnapHelper()
        //     snapHelper.attachToRecyclerView(binding.rvCarouselTrendingMovies)
        mediaTrendingAdapter = MediaTrendingAdapter()
        binding.rvCarouselTrendingMovies.adapter = mediaTrendingAdapter
    }

    private fun handleLoader(showLoader: Boolean) {
        binding.rvCarouselTrendingMovies.isVisible = !showLoader
        binding.pbHome.isVisible = showLoader
    }

    private fun handleButtonToggleListener() {
        binding.btgSelector.addOnButtonCheckedListener { group, checkedId, isChecked ->
            when (checkedId) {
                R.id.btn_movies -> {
                    if (isChecked) viewModel.retrieveTrendingMovies()
                }

                R.id.btn_tv_series -> {
                    if (isChecked) viewModel.retrieveTrendingTvShows()
                }
            }
        }
    }

}