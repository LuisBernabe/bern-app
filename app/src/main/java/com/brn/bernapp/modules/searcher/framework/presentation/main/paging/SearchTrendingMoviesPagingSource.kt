package com.brn.bernapp.modules.searcher.framework.presentation.main.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.brn.bernapp.modules.searcher.usecases.GetMoviesToDiscoverUseCase
import com.brn.bernapp.shared.domain.model.Movie
import retrofit2.HttpException
import java.io.IOException

class SearchTrendingMoviesPagingSource(private val getMoviesToDiscoverUseCase: GetMoviesToDiscoverUseCase) :
    PagingSource<Int, Movie>() {
    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        Log.d(TAG, "load- Params: $params")

        return try {
            val nextNumberPage = params.key ?: 1
            val response = getMoviesToDiscoverUseCase.execute(nextNumberPage)
            Log.i(TAG, "Response: $response")
            LoadResult.Page(data = response, prevKey = null, nextKey = nextNumberPage + 1)
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

    companion object {
        const val TAG = "SEARCH-TRENDING-MOVIES-PS"
    }
}