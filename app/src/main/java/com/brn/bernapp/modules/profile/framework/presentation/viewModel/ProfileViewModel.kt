package com.brn.bernapp.modules.profile.framework.presentation.viewModel

import com.brn.bernapp.shared.framework.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor() : BaseViewModel() {
}