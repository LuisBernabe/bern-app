package com.brn.bernapp.modules.searcher.framework.presentation.main.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.brn.bernapp.modules.searcher.framework.presentation.main.holder.MovieToDiscoverHolder
import com.brn.bernapp.shared.domain.model.Movie

class MovieToDiscoverAdapter(val onClickListener: (movie: Movie) -> Unit) :
    PagingDataAdapter<Movie, MovieToDiscoverHolder>(MovieToDiscoverDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieToDiscoverHolder {
        return MovieToDiscoverHolder.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: MovieToDiscoverHolder, position: Int) {
        getItem(position)?.let {
            holder.binding(it, onClickListener)
        }
    }


}

object MovieToDiscoverDiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

}