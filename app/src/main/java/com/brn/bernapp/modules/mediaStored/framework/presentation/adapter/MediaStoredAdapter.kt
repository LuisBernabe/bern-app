package com.brn.bernapp.modules.mediaStored.framework.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.brn.bernapp.modules.searcher.framework.presentation.main.holder.MediaStoredHolder
import com.brn.bernapp.shared.domain.model.Media

class MediaStoredAdapter : ListAdapter<Media, MediaStoredHolder>(MediaStoredDiffUtil) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaStoredHolder {
        return MediaStoredHolder.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: MediaStoredHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }
}


object MediaStoredDiffUtil : DiffUtil.ItemCallback<Media>() {
    override fun areItemsTheSame(oldItem: Media, newItem: Media): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Media, newItem: Media): Boolean {
        return oldItem.id == newItem.id && oldItem.backdropPath == newItem.backdropPath
    }

}