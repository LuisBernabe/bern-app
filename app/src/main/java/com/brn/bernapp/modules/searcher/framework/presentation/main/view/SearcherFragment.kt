package com.brn.bernapp.modules.searcher.framework.presentation.main.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brn.bernapp.R
import com.brn.bernapp.databinding.FragmentSearcherBinding
import com.brn.bernapp.modules.searcher.framework.presentation.main.adapter.MovieToDiscoverAdapter
import com.brn.bernapp.modules.searcher.framework.presentation.main.viewModel.SearcherViewModel
import com.brn.bernapp.shared.framework.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearcherFragment : BaseFragment<FragmentSearcherBinding>() {
    private val viewModel: SearcherViewModel by viewModels()
    private lateinit var movieToDiscoverAdapter: MovieToDiscoverAdapter


    override fun startBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSearcherBinding.inflate(layoutInflater)

    override fun initView() {
        super.initView()
        binding.fabStartList.hide()
        setupRvMovies()
        viewModel.movieListPaging.observe(viewLifecycleOwner) {
            it?.let {
                movieToDiscoverAdapter.submitData(viewLifecycleOwner.lifecycle, it)
            }
        }
        viewModel.setup()
        binding.fabStartList.setOnClickListener {
            binding.rvMovies.scrollToPosition(0)
            binding.fabStartList.hide()
        }

        //findNavController().navigate(R.id.action_searcherFragment_to_detailsFragment)

    }


    private fun setupRvMovies() {
        binding.rvMovies.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        movieToDiscoverAdapter = MovieToDiscoverAdapter(onClickListener = {
            Log.i("SearcherFragment", "movieId: ${it.id}")
            viewModel.downloadMovieInfo(it)
            findNavController().navigate(R.id.action_searcherFragment_to_detailsFragment)
        })
        binding.rvMovies.adapter = movieToDiscoverAdapter

        binding.rvMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (binding.rvMovies.canScrollVertically(-1))
                    binding.fabStartList.show()
                else
                    binding.fabStartList.hide()
            }
        })
    }
}