package com.brn.bernapp.modules.mediaStored.framework.presentation.viewModel

import com.brn.bernapp.shared.framework.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MediaStoredViewModel @Inject constructor() : BaseViewModel() {

}